package gob.hidalgo.curso.components.generales;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import gob.hidalgo.curso.database.generales.ClienteEO;
import gob.hidalgo.curso.database.generales.ConceptoEO;
import gob.hidalgo.curso.database.generales.OrdenPagoEO;
import gob.hidalgo.curso.utils.Modelo;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component("OrdenesPagosC")
public class OrdenesPagosC {
	
	@Autowired
	private SqlSession sqlSession;

	public OrdenesPagosC() {
		super();
		log.debug("Se Crea Componente OrdenesPagosC");
	}
	
	public OrdenPagoEO nuevo() {
		return new OrdenPagoEO();
	}
	
	public Modelo<OrdenPagoEO> modelo( ClienteEO cliente ){
		List<OrdenPagoEO> listado=new LinkedList<>();
		listado = sqlSession.selectList("generales.ordenesPagos.listado", cliente);
		return new Modelo<>(listado);
	}
	
	public boolean guardar( OrdenPagoEO orden, ClienteEO cliente,  Modelo<ConceptoEO> modelo) {
		switch (orden.getEstatus()) {
			case PAGADO:
				orden.setFechaPago(LocalDateTime.now());
				orden.setMotivoCancelacion(null);
			break;
			case CANCELADO:
				orden.setFechaCancelacion(LocalDateTime.now());
			break;
			case PENDIENTE:
				orden.setMotivoCancelacion(null);
			break;
		}
		if (orden.getId()==null) {
			HashMap<String, Object> mapaParametros = new HashMap<String, Object>();
			mapaParametros.put("orden", orden);
			mapaParametros.put("cliente", cliente);
			sqlSession.insert("generales.ordenesPagos.insertar", mapaParametros);
		}
		else {
			sqlSession.update("generales.ordenesPagos.actualizar", orden);
			sqlSession.delete("generales.conceptos.eliminarOrden", orden);
		}
		for (ConceptoEO fila : modelo.getListado()) {
			HashMap<String, Object> mapaParametros=new HashMap<String, Object>();
			mapaParametros.put("orden", orden);
			mapaParametros.put("concepto", fila);
			sqlSession.insert("generales.conceptos.insertar", mapaParametros);
		}
		return true;
	}
	
	public boolean eliminar( OrdenPagoEO orden) {
		log.debug("eliminar : {}", orden);
		sqlSession.delete("generales.ordenesPagos.eliminar", orden);
		return true;
	}
	
	public Modelo<ConceptoEO> modeloConceptosVacio(){
		List<ConceptoEO> listado=new LinkedList<>();
		return new Modelo<>(listado);
	}	
	
	public Modelo<ConceptoEO> modeloConceptos( OrdenPagoEO orden ){
		List<ConceptoEO> listado=new LinkedList<>();
		listado = sqlSession.selectList("generales.conceptos.listado", orden);
		return new Modelo<>(listado);
	}	
	
	public ConceptoEO nuevoConcepto() {
		return new ConceptoEO();
	}
	
	public Modelo<ConceptoEO> agregadoConcepto( OrdenPagoEO orden, Modelo<ConceptoEO> modelo, ConceptoEO concepto ) {
		boolean edicion=false;
		Integer idAux=0;
		BigDecimal montoTotal=BigDecimal.ZERO;
		for (ConceptoEO fila : modelo.getListado()) {
			if (fila.equals(concepto)) {
				edicion=true;
			}
			if (fila.getId()<0) {
				idAux=fila.getId();
			}
			montoTotal=montoTotal.add(fila.getMontoTotal());
		}
		log.debug("Monto total Asignado Por Funcion : {}", montoTotal);
		if (edicion) {
			orden.setMontoTotal(montoTotal);
			return modelo;	
		}
		else {
			concepto.setId(idAux-1);
			modelo.getListado().add(concepto);
			montoTotal=montoTotal.add(concepto.getMontoTotal());
			orden.setMontoTotal(montoTotal);
			return modelo;
		}
	}
}