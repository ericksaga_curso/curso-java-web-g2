package gob.hidalgo.curso.components;

import java.util.Arrays;

import org.springframework.stereotype.Component;

import gob.hidalgo.curso.database.dominios.EstatusOrdenPagoDO;
import gob.hidalgo.curso.utils.Modelo;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component("DominiosC")
public class DominiosC {
	
	public DominiosC() {
		super();
		log.debug("Se crea componente DominiosC");
	}

//	public Modelo<GenerosDO> generos() {
//		return new Modelo<>(Arrays.asList(GenerosDO.values()));
//	}


	public Modelo<EstatusOrdenPagoDO> estatusOrdenPago() {
		return new Modelo<>(Arrays.asList(EstatusOrdenPagoDO.values()));
	}
}