package gob.hidalgo.curso.components.generales;

import java.util.LinkedList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import gob.hidalgo.curso.database.generales.ClienteEO;
import gob.hidalgo.curso.utils.Modelo;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component("ClientesC")
public class ClientesC {
	
	@Autowired
	private SqlSession sqlSession;
	
	public ClientesC() {
		super();
		log.debug("Se crea el Componete ClientesC");
	}
	
	public ClienteEO nuevo() {
		return new ClienteEO();
	}
	
	public Modelo<ClienteEO> modelo(){
		List<ClienteEO> listado=new LinkedList<>();
		listado = sqlSession.selectList("generales.clientes.listado");
		return new Modelo<ClienteEO>(listado);
	}
	
	public boolean guardar(ClienteEO cliente) {
		if (cliente.getId()==null) {
			sqlSession.insert("generales.clientes.insertar",   cliente);
		}
		else {
			sqlSession.update("generales.clientes.actualizar", cliente);
		}
		return true;
	}
	
	public boolean eliminar(ClienteEO cliente) {
		sqlSession.delete("generales.clientes.eliminar", cliente);
		return true;
	}
}
