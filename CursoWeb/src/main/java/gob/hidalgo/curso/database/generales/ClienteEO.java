package gob.hidalgo.curso.database.generales;

import java.io.Serializable;
import java.time.LocalDate;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;

import org.apache.ibatis.type.Alias;

import gob.hidalgo.curso.utils.EntityObject;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Alias("ClienteEO")
public class ClienteEO extends EntityObject implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String curp;
	
	@Email(message = "Favor de anotar un E-Mail v�lido")
	private String mail;
	
	@NotBlank(message = "Favor de anotar el Nombre")
	private String nombre;
	
	@NotBlank(message = "Favor de anotar el Primer Apellido")
	private String primerApellido;
	
	@NotBlank(message = "Favor de anotar el Segundo Apellido")
	private String segundoApellido;
	
	@NotNull(message = "Favor de anotar la Fecha de Nacimiento")
	@PastOrPresent(message = "Favor de anotar una Fecha de Nacimiento v�lida")
	private LocalDate fechaNacimiento;
	
	@NotBlank(message = "Favor de anotar el Celular")
	private String celular;
}
