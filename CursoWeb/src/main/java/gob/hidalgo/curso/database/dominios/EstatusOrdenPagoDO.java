package gob.hidalgo.curso.database.dominios;

import org.apache.ibatis.type.Alias;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
@Alias("EstatusOrdenPagoDO")
public enum EstatusOrdenPagoDO {
	PENDIENTE("Pendiente"),
	PAGADO("Pagado"),
	CANCELADO("Cancelado");
	
	private String nombre;
}
