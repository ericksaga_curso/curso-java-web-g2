package gob.hidalgo.curso.database.generales;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PastOrPresent;
import org.apache.ibatis.type.Alias;
import gob.hidalgo.curso.database.dominios.EstatusOrdenPagoDO;
import gob.hidalgo.curso.utils.EntityObject;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Alias("OrdenPagoEO")
public class OrdenPagoEO extends EntityObject implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Integer 				id;
	@NotBlank(message = "Favor De Anotar El Folio")
	private String					folio;
	private LocalDateTime			fechaCreacion;
	private String					fechaCreacionF;
	@PastOrPresent
	private LocalDateTime			fechaPago;
	@PastOrPresent
	private LocalDateTime			fechaCancelacion;
	private BigDecimal				montoTotal;
	private String					motivoCancelacion;
	private EstatusOrdenPagoDO		estatus;
	
	public OrdenPagoEO() {
		super();
		this.montoTotal=BigDecimal.ZERO;
		this.estatus=EstatusOrdenPagoDO.PENDIENTE;
		fechaCreacion=LocalDateTime.now();
	}
	/*   Funcion para formatear la fecha */
	public String getFechaCreacionF() {
		if (fechaCreacionF==null) {
			fechaCreacionF=(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")).format(this.fechaCreacion);
		}
		return fechaCreacionF;
	}
}