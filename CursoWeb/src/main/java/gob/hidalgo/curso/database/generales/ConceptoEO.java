package gob.hidalgo.curso.database.generales;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import org.apache.ibatis.type.Alias;
import gob.hidalgo.curso.utils.EntityObject;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Alias("ConceptoEO")
public class ConceptoEO extends EntityObject implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Integer 		id;
	@NotBlank
	private String 			concepto;
	@NotNull
	private Integer 		cantidad;
	@NotNull
	private BigDecimal 		montoUnitario;
	@NotNull
	private BigDecimal 		montoTotal;
	private UnidadMedidaEO	unidadMedida;
	
	public ConceptoEO() {
		montoTotal=BigDecimal.ZERO;
		montoUnitario=BigDecimal.ZERO;
		cantidad=0;
	}
	
	public BigDecimal getMontoTotal() {
		//if (montoTotal.equals(BigDecimal.ZERO)) {
		montoTotal=montoUnitario.multiply(new BigDecimal(cantidad));
		//}
		return montoTotal;
	}
}